\begin{abstract}
	Enriched monads on the category $\UMet$ of ultrametric spaces and nonexpanding maps are investigated.
	This category is cartesian closed and locally countably presentable.
	We prove a bijective correspondence between strongly countably accessible monads and varieties of $\Sigma$-algebras in $\UMet$ for all classical signatures $\Sigma$ with countable arities.
	For the more general signatures $\Omega$ in the sense of Kelly and Power there is a bijective correspondence between enriched $\lambda$-accessible monads and varieties of $\Omega$-algebras, where $\UMet$ is viewed as a locally $\lambda$-presentable category and $\Omega$ has $\lambda$-ary generalized arities.
\end{abstract}

\maketitle

\section{Introduction}

Algebras acting on metric spaces have recently received a lot of attention in connection with the semantics of probabilistic computation, see e.g.\ [CITE].
Mardare, Panangaden and Plotkin worked in [CITE] with $\Sigma$-algebras in the category $\Met$ of metric spaces and nonexpanding maps, where $\Sigma$ is a signature with countable arities.
They introduced \emph{quantitative equations} which are expressions $t =_{\epsilon} t'$ where $t$ and $t'$ are terms and $\epsilon \geq 0$ is a rational number.
A metric algebra $A$ satisfies this equation iff for every interpretation of the variables the computation of $t$ and $t'$ yields elements of distance at most $\epsilon$ in $A$.
Example: almost commutative monoids are metric monoids in wthich, for a given constant $\epsilon$, the distanc of $ab$ and $ba$ is at most $\epsilon$.
This is presented by the quantitative equation $xy =_{\epsilon} yx$.

We call classes of metric $\Sigma$-algebras that can be presented by a set of quantitative equations \emph{varieties} (in [CITE] they are called $1$-varieties).
Every variety $\Vvar$ has a free algebra on each metric space, which yields a corresponding monad $\Tmon_\Vvar$ on $\Met$.
Moreover, $\Vvar$ is isomorphic to the Eilenberg-Moore category $\Met^{\Tmon_\Vvar}$.
Can one characterize monads on $\Met$ that correspond to varieties?
One property is that such a monad is countably accessible, i.e.\ it preerves countably directed colimits.
Another property is that it is enriched, that is, locally nonexpanding.
Unfortunately, these conditions are not sufficient (Example [LINK]).

In our paper we restrict the spaces we work with to ultrametric ones: those satisfying the stronger triangle inequality
\[
d(x,y) \leq d(x,z) \vee d(z,y).
\]
For the category $\UMet$ of ultrametric spaces and nonexpanding maps we prove that a monad $\Tmon$ corresponds to a variety iff it is \emph{strongly countably accessible}.
This means that for the full embedding $J: \Set_c \hookrightarrow \UMet$ of countable sets (as discrete spaces) the functor $T$ is a left Kan extension of its restriction to $\Set_c$:
\[
T = \Lan{J}{T \comp J}.
\]
This immediately generalizes from countable arities to arities smaller than $\lambda$, for every uncountable regular cardinal $\lambda$: let $J$ denote the embedding of sets of cardinality less than $\lambda$ to $\UMet$.
Then the monads corresponding to varieties of $\lambda$-ary ultrametric algebras are precisely the strongly $\lambda$-accessible monads: those with $T = \Lan{J}{T \comp J}$.

For general $\lambda$-accessible enriched monads on $\UMet$ we also present a semantic characterization.
Here we work with types $\Omega$ of algebras more general than the classical signatures.
This follows the idea of Kelly and Power [CITE] who presented enriched finitary monads on a symmetric monoidal closed category $\K$ which is locally finitely presentable in the enriched sense.
For that presentation they used types where arities are not cardinals but objects of $\K_\fp$, a set representing all finitely presentable objects up to isomorphism.
Then a type is a collection $\Omega = (\Omega_n)_{n \in \K_\fp}$ of objects of $\K$.
And a $\Omega$-algebra is ana object $A$ of $\K$ together with morphisms
\[
\Omega_n \tensor [n,A] \to A \text{for all $n \in \K_\fp$.}
\]
This approach generalizes to locally $\lambda$-presentable categories in the enriched sense: instead of $\K_\fp$ one takes a set $\K_\lambda$ representing all  $\lambda$-presentable objects.
And one obtains a presentation of all enriched $\lambda$-accessible monads on $\K$.

In summary, we establish a bijective correspondence, for every uncountable cardinal $\lambda$, between
\begin{enumerate}
	\item
	enriched $\lambda$-accessible monads on $\UMet$ and varieties of ultrametric $\Omega$-algebras, and
	\item
	enriched $\lambda$-accessible monads on $\UMet$ and varieties of $\Omega$-algebras.
\end{enumerate}

There are good reasons for working with ultrametric spaces.
The larger category $\Met$ is not cartesian closed.
It has the symmetric monoidal closed structure with $A \tensor B$ given by the cartesian product where
\[
d((a,b),(a',b')) = d(a,a') + d(b,b').
\]
Thus e.g.\ a monoid in the monoidal category $\Met$ is a broader concept than a metric monoid in the sense of [CITE], where the multiplication is required to be nonexpanding from $A \times A$ (with the maximum metric) to $A$.

Another reason is that for $\omega$-varieties (see [\dots]) a complete characterization of the corresponding monads on $\UMet$ has been presented in [CITE], whereas no characterization for $\Met$ is known.

\paragraph{Related results}
For ordered $\Sigma$-algebras closely related results have been presented recently.
Strongly finitary monads on $\Pos$ bijectively correspond to varieties of finitary ordered $\Sigma$-algebras: see [CITE], for a shorter proof see [CITE].
And enriched finitary monads bijectively correspond to varieties of ordered $\Omega$-algebras [CITE].

Enriched $\lambda$-accessible monads on $\Met$ have also been semantically characterized by Rosický [CITE] who uses a different concept of a type of algebras.

\section{Strongly countably accessible monads}

\begin{notation}
	\begin{enumerate}
		
		\item
		By an \emph{ultrametric space} we always mean the extended variant: distance $\infty$ is allowed.
		Thus, an ultrametric on a set $X$ is a mapping
		\[
		d: X \times X \to [0, +\infty]
		\]
		satisfying $d(x, y) = 0$ iff $x = y$;
		$d(x, y) = d(y, x)$ and the following (ultrametric) triangle inequality
		\[
		d(x,z) \leq \max \{ d(x, y), d(y, z) \}.
		\]
		
		\item
		We denote by $\UMet$ the category of ultrametric spaces and nonexpanding functions $f: X \to Y$: for all $x, x' \in X$ we have $d(x, x') \geq d(f(x), f(x'))$.
		
	\end{enumerate}
\end{notation}

\begin{example}
	\begin{enumerate}
		
		\item The free monoid $X*$ (of finite words in a set $X$) yields the following ultrametrics:
		the distance of words $u \neq v$ is
		\[
		d(u, v) = 2^{-n}
		\]
		for the largest $n \in \N$ such that the prefixes of $u$ and $v$ of length $n$ coincide.
		
		\item
		The set of all binary trees (up to isomorphism) is an ultrametric space with the distance of trees $u \neq v$
		\[
		d(u, v) = 2^{-n}
		\]
		for the largest $n \in \N$ such that the cuttings of $u$ and $v$ at height $n$ coincide.
		
		\item
		Every set $X$ is considered as the \emph{discrete space} with $d(x, y) = \infty$ for all $x \neq y$.
		This is an ultrametric.
		
		\item
		Let $M$ be an ultrametric space.
		The set $\Pow_\fp M$ of finite subsets of $M$ carries the Hausdorff metric\footnote{Hausdorff considered only nonempty subsets.
			Since we work with extended ultrametrics, there is no problem with $\emptyset$: given $B \neq \emptyset$ we have $d(\emptyset,B) = \infty$.}
		\[
		d(A, B) = \bigvee_{a \in A} d(a, B) \vee \bigvee_{b \in B} d(b, A)
		\]
		where the distance between an element $a$ and a set $B$ is $\bigwedge_{b \in B} d(a, b)$.
		This is an ultrametric: for all $A,B,C \in \Pow_\sf M$ we prove
		\[
		d(A, C) \leq d(A, B) \vee d(B, C).
		\]
		This is clear if $B = \emptyset$ since $d(A, \emptyset) = \infty$.
		Else choose $b_0 \in B$.
		For each $a \in C$ we then have
		\[
		d(a, C) \leq d(A, B) \vee d(B, C)
		\]
		since given $c \in C$, we compute
		\[
		d(a, c) \leq d(a, b_0) \vee d(b_0, c) \leq d(A, B) \vee d(B, C).
		\]
		From this the triangle inequality follows.
		
		\item
		A product of ultrametric spaces $X_i$ (with $i \in I$) is their cartesian product with the supremum metric:
		the distance of $(x_i)_{i \in I}$ and $(y_i)_{i \in I}$ is $\bigvee_{i \in I} d(x_i, y_i)$.
		
		A coproduct is the disjoint union $\Coprod_{i \in I} X_i$ with each $X_i$ as a subspace and with distance $\infty$ for two elements of distinct summands.
		
		\item
		The space $[A, B]$ of all morphisms $f: A \to B$ between ultrametric spaces with the supremum metric is an ultrametric space.
		And [CUTOFF].
		Thus $\UMet$ is a cartesian closed category.
		
		\item
		Real numbers are not an ultrametric space.
		But $2$-adic reals are.
		This is the Cauchy completion of rational numbers with the distance of numbers $u \neq v$ defined by
		\[
		d(u, v) = 2^{-n}
		\]
		for the unique $n$ with $|u - v| = 2^{-n} \cdot \frac{p}{q}$ with $p$ and $q$ odd.
	\end{enumerate}
\end{example}

\begin{remark}
	\begin{enumerate}
		
		\item
		The category $\UMet$ is cartesian closed;
		products $\prod_{i \in I} X_i$ are cartesian products with the supremum metric:
		\[
		d((x_i)_{i \in I},(y_i)_{i \in I}) = \bigvee_{i \in I} d(x_i, y_i).
		\]
		Function spaces are the hom-sets
		\[
		[X, Y] = \UMet(X, Y)
		\]
		with the supremum metric:
		\[
		d(f, g) = \bigvee_{x \in X} d(f(x), g(x))
		\]
		
		\item
		\emph{Enriched} endofunctors $T$ of $\UMet$ are locally nonexpanding: $d(Tf, Tg) \leq d(f, g)$ for every parallel pair $f$, $g$ of morphisms.
		
		In contrast: enriched natural transformations (between enriched functors) are just the ordinary ones.
		
		\item
		An \emph{ultrametric enriched category} is a category $\K$ enriched over $\UMet$: every hom-set carries an ultrametric such that composition is nonexpanding.
		
		We recall the concept of a \emph{weighted colimit} $\Colim{W}{D}$.
		Given a diagram $D: \D \to \K$ and a weight $W: \D^\op \to \UMet$, a weighted colimit is an object $C = \Colim{W}{D}$ together with an isomorphism
		\[
		\psi_X: \K(C,X) \to [\D^\op,\UMet](W, \K(D \blank, X))
		\]
		natural in $X \in \K$.
		
		\item
		The category $\UMet$ has all weighted colimits.
		Inded, [INSERT ARGUMENT]
		
	\end{enumerate}
\end{remark}

\begin{example}
	\begin{enumerate}
		\item
		\emph{Directed colimits} are conical colimits (colimits weighted by the weight constant at $\One$, the terminal space) where the domain $\D$ is a directed poset.
		As an example, consider $\D = \N$, linearly ordered, and $Dn = \{ 0, 1 \}$ with distance $d(a, b) = \frac{1}{n}$, where the connecting maps are all $\id_{\{a, b\}}$.
		The colimit of $\D$ is a singleton space.
		
		This shows that the only finitely presentable ultrametric space is $\emptyset$:
		for $X \neq \emptyset$ the hom-functor $[X, \blank]$
	\end{enumerate}
\end{example}